<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Galery</title>

	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
    </style>
</head>
<body id="app-layout">

   <div class="container">
		<div class="page-header">
			<div class="pull-right">
				<a href="/create" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Novo visitante</a>
			</div>

			<h1>Galeria <small>Listagem</small></h1>
		</div>

		<table id="index-grid" class="data-grid table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>Ingresso</th>
					<th>Nome</th>
					<th>Data da Visita</th>
					<th>Exposição</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
				<?php foreach ($visitors as $visitor) { ?>
				<tr>
					<td><?php echo sha1($visitor['id']) ?></td>
					<td><?php echo $visitor['nome'] ?></td>
					<td><?php echo (new DateTime($visitor['data_visita']))->format('d/m/Y ') ?></td	>
					<td><?php echo $visitor['exposicao'] ?></td>
					<td>
						<a href="/edit/<?php echo $visitor['id'] ?>" class="btn btn-success" title="Edit" >
							<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
						</a>
						<a href="/delete/<?php echo $visitor['id'] ?>" class="btn btn-danger" title="Delete" >
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
						</a>

					</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>

		<div class="page-footer row">
			<div class="btn-group btn-group-lg pull-right" role="group" aria-label="Large button group">
				<a type="button" class="btn btn-default change-page disabled" id="prev-page" data-offset="0">
					<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
				</a>

				<a type="button" class="btn btn-default change-page" id="next-page" data-offset="1">
					<span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
				</a>
			</div>
		</div>
		<hr>
	</div>

    <!-- JavaScripts -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>