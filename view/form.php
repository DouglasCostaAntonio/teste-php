<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Galery</title>

    <!-- Fonts -->

    <!-- Styles -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <style>
    </style>
</head>
<body id="app-layout">
   <div class="container">
		<form id="supplier-form" class="form-horizontal" action="<?php echo $action ?>" method="post" accept-char="UTF-9" autocomplete="off" enctype="multipart/form-data">

			<div class="page-header">
				<div class="pull-right">
					<a href="/" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> Voltar</a>
				</div>

				<h1>Galery <small>Novo visitante</small></h1>
			</div>

			<div class="form-group row">
				<label for="nome" class="col-lg-3 control-label">Nome</label>

				<div class="col-lg-5">
					<input type="text" class="form-control" name="nome" id="nome" placeholder="Nome" value="<?php echo !empty($visitor)? $visitor['nome'] : '' ?>" required="required">

					<span class="help-block">
						Informe o nome do visitante
					</span>
				</div>
			</div>

			<div class="form-group row">
				<label for="rg" class="col-lg-3 control-label">RG</label>

				<div class="col-lg-5">
					<input type="text" class="form-control" name="rg" id="rg" placeholder="rg" value="<?php echo !empty($visitor)? $visitor['rg'] : '' ?>" required="required">

					<span class="help-block">
						Informe o rg do visitante
					</span>
				</div>
			</div>

			<div class="form-group row">
				<label for="cpf" class="col-lg-3 control-label">CPF</label>

				<div class="col-lg-5">
					<input type="text" class="form-control" name="cpf" id="cpf" placeholder="cpf" value="<?php echo !empty($visitor)? $visitor['cpf'] : '' ?>" required="required">

					<span class="help-block">
						Informe o cpf do visitante
					</span>
				</div>
			</div>

			<div class="form-group row">
				<label for="exposicao" class="col-lg-3 control-label">Exposição desejada</label>

				<div class="col-lg-5">
					<select class="form-control date-time" name="exposicao" id="exposicao">
						<option></option>
						<option value="XV Feira do Livro">XV Feira do Livro</option>
						<option value="Anarquismo – Fatos em Fotos">Anarquismo – Fatos em Fotos</option>
					</select>

					<span class="help-block">
						Informe a exposição desejada
					</span>
				</div>
			</div>

			<div class="form-group row">
				<label for="data_nascimento" class="col-lg-3 control-label">Data de Nascimento</label>

				<div class="col-lg-5">
					<input type="date" class="form-control date-time" name="data_nascimento " id="data_nascimento" placeholder="data_nascimento" value="<?php echo !empty($visitor)? $visitor['data_nascimento'] : '' ?>" required="required">

					<span class="help-block">
						Informe o Data de Nascimento do visitante
					</span>
				</div>
			</div>

			<div class="form-group row">
				<label for="data_visita" class="col-lg-3 control-label">Data de Visita</label>

				<div class="col-lg-5">
					<input type="date" class="form-control date-time" name="data_visita" id="data_visita" placeholder="data_visita" value="<?php echo !empty($visitor)? $visitor['data_visita'] : '' ?>" required="required">

					<span class="help-block">
						Informe o Data de Visita do visitante
					</span>
				</div>
			</div>

			<div class="page-footer ">
				<button class="btn btn-primary btn-lg" type="submit"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Salvar</button>
			</div>

			<br>

			</form>
		</div>
	</div>

    <!-- JavaScripts -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/locale/br.js"></script>

	<script type="text/javascript">
		const TestaCPF = function(strCPF) {
		    var Soma;
		    var Resto;
		    Soma = 0;
			if (strCPF == "00000000000") return false;

			for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
			Resto = (Soma * 10) % 11;

		    if ((Resto == 10) || (Resto == 11))  Resto = 0;
		    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

			Soma = 0;
		    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
		    Resto = (Soma * 10) % 11;

		    if ((Resto == 10) || (Resto == 11))  Resto = 0;
		    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
		    return true;
		}

		$('#cpf').on('blur', function(e) {
			var cpf = $(this).val();

			if (!TestaCPF(cpf)) {
				alert('cpf invalido');

				$(this).val('');
			}

		});

        $('.date-time').on('change', function(e) {
            var data_nascimento = $('#data_nascimento').val();
            var data_visita = $('#data_visita').val();
            var exposicao = $('#exposicao').val()

            if (!data_nascimento || !data_visita || exposicao != 'Anarquismo – Fatos em Fotos') {
                return true;
            }

            if ((moment(data_visita).year() - moment(data_nascimento).year()) < 18) {
                alert('Visitante nao autorizado');
            	$('#data_nascimento').val('');

            }
        });
	</script>
</body>
</html>