# Teste Programador PHP #

Uma galeria de arte solicitou o desenvolvimento de um sistema para controle de visitantes, onde serão registradas as pessoas que visitam as mostras de artes que estão em exposição.
O visitante deverá informar:

* Nome
* RG
* CPF (deve ser válido)
* Data de Nascimento
* Data da Visita
* Exposição desejada

No momento existem duas mostras acontecendo, a "XV Feira do Livro" e a "Anarquismo – Fatos em Fotos".
Caso o visitante esteja se registrando para a mostra "Anarquismo – Fatos em Fotos" o sistema deve validar se o mesmo é maior de 18 anos, devido ao teor de algumas fotografias expostas.
O sistema deverá gerar um código único que identifique a visita agendada, que será o "ingresso" do visitante.
Essas informações devem ser gravadas no banco de dados.