<?php
	namespace Triata;

	$configs = include('config.php');

	Class Visitor
	{
		protected $configs;
		protected $data = [];

		protected $fields = [
			'nome',
			'rg',
			'cpf',
			'exposicao',
			'data_nascimento',
			'data_visita',
		];

		public function __construct() {
			$this->configs = include('config.php');
		}

		public function all() {
			$link = $this->getConnection();
			$statement = $link->prepare("select * from visitor");
			$statement->execute();

			return $statement->fetchAll();
		}

		public function find($id) {
			$link = $this->getConnection();
			$statement = $link->prepare("select * from visitor where id = :id");
			$statement->execute([':id' => $id]);
			return $statement->fetch();
		}

		public function fill($data){
			// sanitize
			$this->data = $data;
		}

		public function save() {
			$link = $this->getConnection();

			$statement = $link->prepare("INSERT INTO visitor(".implode(',', $this->fields).")
			    VALUES(:".implode(',:', $this->fields).")");

			$statement->execute($this->data);

			return true;
		}

		public function update($id) {
			$link = $this->getConnection();

			$fields = [];

			foreach ($this->fields as $field) {
				$fields[] = $field.' = :'.$field;
			}

			$stmt = $link->prepare("UPDATE visitor SET ".implode(',', $fields)."
            WHERE id = :id");

			foreach ($this->data as $key => $data) {
				$stmt->bindParam(':'.$key, $data, \PDO::PARAM_STR);
			}

			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->execute();

			return true;
		}

		public function delete($id) {
			$link = $this->getConnection();

			$sql = "DELETE FROM visitor WHERE id =  :id";
			$stmt = $link->prepare($sql);
			$stmt->bindParam(':id', $id, \PDO::PARAM_INT);
			$stmt->execute();

			return true;
		}

		protected function getConnection() {
			return new \PDO('mysql:host='.$this->configs->host.';dbname='.$this->configs->database.';charset=utf8mb4', $this->configs->username, $this->configs->pass);
		}
	}

?>