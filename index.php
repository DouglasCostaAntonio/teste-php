<?php
	include('Gallery.php');

	use Triata\Gallery;

	$parameters = explode('/', $_SERVER['REQUEST_URI']);

	$method = $parameters[1]?:'index';

	$parameter = isset($parameters[2])?$parameters[2]:null;

	$gallery = new Gallery();

	echo $gallery->{$method}($parameter);
?>