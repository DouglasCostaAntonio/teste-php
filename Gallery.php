<?php
namespace Triata;
include('View.php');
include('Visitor.php');


use Triata\View;
use Triata\Visitor;

Class Gallery
{
	protected $configs;

	public function __construct() {
		$this->configs = include('config.php');
		date_default_timezone_set('UTC');

	}

	public function index() {
		$visitor = new Visitor();

		$visitors = $visitor->all();

		$view = new View();

		return $view->render('view/list.php', compact('visitors'));
	}

	public function create() {
		$view = new View();

		$visitor = [];

		$action = 'store';

		return $view->render('view/form.php', compact('visitor', 'action'));
	}

	public function store() {
		$visitor = new Visitor();
		$visitor->fill($_POST);
		$visitor->save();

		$this->redirect('/');
	}

	public function edit($id) {
		$_visitor = new Visitor();

		$visitor = $_visitor->find($id);

		$view = new View();

		$action = 'http://'.$_SERVER['HTTP_HOST'].'/update/'.$id;

		return $view->render('view/form.php', compact('visitor', 'action'));
	}

	public function update($id) {
		$visitor = new Visitor();
		$visitor->fill($_POST);

		$visitor->update($id);

		$this->redirect('/');
	}

	public function delete($id) {
		$_visitor = new Visitor();

		$visitor = $_visitor->delete($id);

		$this->redirect('/');
	}

	protected function redirect($route) {
		header("Location: ".$route."");
		die();
	}
}
?>